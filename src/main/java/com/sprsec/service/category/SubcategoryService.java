package com.sprsec.service.category;

import com.sprsec.model.Category;
import com.sprsec.model.Subcategory;

/**
 * Created by Yaroslav on 04.02.2015.
 */
public interface SubcategoryService {
    public void addSubcategory(Subcategory subcategory);
    public Subcategory getCategoryByName(String name);
}
